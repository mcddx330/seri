// node seri.js

// 最後の配列を取得
/*
Array.prototype.getLastVal = function (){
  return this[this.length -1];
}
*/

const HELPDOC = `
・返事系
　・せりりん, セリリン
　・にｐ, にぱー, nip
・直近のツイート
　・今のせりりん, 今なんていった, 今なんつった
・直筆サイン
　・サインちょうだい, サインくれ
・歌
　・歌って
・セリリン☆マテリアル
　・せりまて, セリマテ, セリリンマテリアル
・喋り
　・喋って, しゃべって
　・はにかむ, はにかんで
・声の強制終了
　・うるさい, やめて, やめろ, だまれ, 黙れ
`;

// ここから
require('date-utils');
const sprintf = require('sprintf');

const FeedParser   = require('feedparser');
const request      = require('request');
const twitter_feed = 'https://queryfeed.net/twitter?q=from%3Aazusagi617&title-type=user-name-both&geocode=&omit-retweets=on&attach=on';
var   feedparser   = new FeedParser({});
var   feed_items   = [];

const fs  = require('fs');
const mp3dir = {
    'serimate': './mp3/serimate/',
    'voice':    './mp3/voice/'
};
var files = [];

const Discord       = require('discord.js');
const client        = new Discord.Client();
const my_id         = '280281865011724289';

client.on('ready', () => {
  console.log('I am ready!');
});

client.on('message', message => {
  var voice_channel = client.channels.get('247365146349928449');
  var is_join_in_voice_channel = (voice_channel.members.get(my_id) !== undefined);

  var msgarr = message.content.split(' ');
  var is_mention = false;

  // メンション判定
  is_mention = /<@280281865011724289>/.test(msgarr[0]);

  // メッセージ整形
  msgarr.shift();
  real_message = msgarr.join('');

  // メンション時のみ反応
  if (is_mention == false) {
    return;
  }

  switch (real_message) {
    case 'h':
      message.reply(HELPDOC);
      break;

    case 'にp':
    case 'にｐ':
    case 'にぱー':
    case 'nip':
      message.reply('はぁｔ💕');
      break;

    case '今のせりりん':
    case '今なんていった':
    case '今なんつった':
      var req = request(twitter_feed);
      req.on('response', function (res) {
          this.pipe(feedparser);
      });

      feedparser.on('readable', function() {
          while(item = this.read()) {
              feed_items.push(item);
          }
      });

      feedparser.on('end', function() {
          var tweet_dt = new Date(feed_items[0].date);
/*
          var attach_url = '';
          if (feed_items[0].enclosures.isEmpty == false) {
              var attach_url = JSON.parse(JSON.stringify(feed_items[0].enclosures[0])).url;
          }
*/
          // 展開防止のため文中のURLを削除
          var twarr = feed_items[0].description.split(' ');
          var tweet = '';
          for(var i in twarr) {
              if (/t.co/.test(twarr[i])) {
                  continue;
              }
              tweet = tweet + twarr[i] + ' ';
          }

          message.channel.send(sprintf(
              "\n%s\n%s\n%s",
              tweet_dt.toFormat('YYYY年 MM月 DD日 HH24時 MI分 SS秒'),
              tweet,
              feed_items[0].permalink
          ));
      });
      break;

    case 'サインちょうだい':
    case 'サインくれ':
      message.reply('はいっ💕', {file: './pict/sign.png'});
      break;

    case '歌って':
    case 'せりまて':
    case 'セリマテ':
    case 'セリリンマテリアル':
      if (is_join_in_voice_channel) {
          voice_channel.leave();
      }
      message.reply('歌うわよ？');

      var list = fs.readdirSync(mp3dir.serimate);
      var keys     = Object.keys(list);
      var filename = list[keys[Math.floor(Math.random() * keys.length)]];

      voice_channel.join()
          .then(connection => {
              const dispatcher = connection.playFile(mp3dir.serimate + filename);
              dispatcher.on("end", () => voice_channel.leave())
          })
          // 常に動くのでコメントアウト
          //.catch(message.reply("やっぱりやめるわ。\n" + console.error));
      break;

    case 'しゃべって':
    case '喋って':
      if (is_join_in_voice_channel) {
          voice_channel.leave();
      }
      message.reply('わかった💋');

      var list     = fs.readdirSync(mp3dir.voice);
      var keys     = Object.keys(list);
      var filename = list[keys[Math.floor(Math.random() * keys.length)]];

      voice_channel.join()
          .then(connection => {
              const dispatcher = connection.playFile(mp3dir.voice + filename);
              dispatcher.on("end", () => voice_channel.leave())
          })
      break;

    case 'はにかみ':
    case 'はにかんで':
      voice_channel.join()
          .then(connection => {
              const dispatcher = connection.playFile(mp3dir.voice + '105226.mp3');
              dispatcher.on("end", () => voice_channel.leave())
          })
      break;

    case 'うるさい':
    case 'やめて':
    case 'やめろ':
    case 'だまれ':
    case '黙れ':
      if (is_join_in_voice_channel === false) {
          message.reply('まだ何も言ってないんだけど💢');
          break;
      }

      message.reply('😢');
      voice_channel.leave();
      break;

    case '':
    case 'せりりん':
    case 'セリリン':
      message.reply('なによー');
      break;

    default:
      break;
  }
});

client.login('MjgwMjgxODY1MDExNzI0Mjg5.C4Hn6Q.cPeUKxwNG02TH5dWqDX-HT2a0SI');
