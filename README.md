# seri [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]
> Seririn (1994-): as know as "Azusa Hinamori, Maika Seririn"; is very terrible daemon, vocalist, voice actor, nurse. Live in Nagoya, Japan.

## Installation

Clone this repository, and run:
```sh
$ npm install
```

## Usage

```js
$ npm run bot
```
## License

 © [Project Oomado]()


[npm-image]: https://badge.fury.io/js/seri.svg
[npm-url]: https://npmjs.org/package/seri
[travis-image]: https://travis-ci.org//seri.svg?branch=master
[travis-url]: https://travis-ci.org//seri
[daviddm-image]: https://david-dm.org//seri.svg?theme=shields.io
[daviddm-url]: https://david-dm.org//seri
